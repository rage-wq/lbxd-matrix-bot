# Lbxd Matrix Bot

Bot which sits in a single Matrix room and adds any movie mentioned to a letterboxd list

### TODO:

* Add/remove from real watchlist
* Set/unset real watched status
* Prevent clobbering reasons in add
  * make getContent return filmData entries
  * make edit accept list of FilmData instead of filmIds
* Allow veto-with-reason
* Implement mention, then add mention to default + watch + veto
* Make veto prevent watchlisting
* Implement uptime + check
* Test if any symbols get clobbered by list-edit
* Make watch, veto, check search iff no URLs are present
* When a film is on the watchlist and is mentioned again, increment a counter
in its notes section.
* Make bulk-add correctly set the watchlist counter
* Re-run bulk-add with current chat logs
* Add !links

## Letterboxd CLI

All this does is log in then add a list of movies to a given list.

Your username/password can be set by entering `lbxd.username` and `lbxd.password`
in `config/default.yml`, or `config/production.yml` if you're worried about committing creds.\
If you're adding to the same list often, you can also set `lbxd.list` to your list URL.

The script supports entering URLs directly, via a file, or by copying from clipboard.

## Matrix Bot

End goal is for this to sit in a Matrix room and add movies to an lbxd list. \
Will also respond to `!watch <url>` and messages beginning `zero days`. \


## TODOs

* General
  * Config schema, plus ability to print pretty schema
  * Improve dev loop
    * look into tsc watch or something
    * source maps
    * tests?
  * For login/lists, ask users if they want it persisted
  * Create config type for easier config debugging
  * Organize code more
* Letterboxd CLI
  * Support more actions- remove etc.
  * Scraping
    * Allows select of list from user's lists
    * Allows use of lbxd's bulk operations-- way kinder than N posts
  * Add daemon mode for eventual use by matrix bot
* Matrix bot
  * Unblock by testing on different home server?
  * Figure out why sync requests keep failing

