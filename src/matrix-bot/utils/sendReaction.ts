
let reactionId = 0;
async function sendReaction(client: any, roomId: any, event: any, emoji = "👀") {
    let encodedRoomId = encodeURIComponent(roomId);
    let body = {
        "m.relates_to": {
            rel_type: "m.annotation",
            event_id: event["event_id"],
            key: emoji,
        },
    };
    let now = (Date.now() / 1000) | 0;
    let transactionId = now + "_lbxd_emoji" + reactionId++;
    await client.doRequest("PUT", `/_matrix/client/r0/rooms/${encodedRoomId}/send/m.reaction/${transactionId}`, null, // qs
    body);
}

export default sendReaction;