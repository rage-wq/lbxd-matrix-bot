require('source-map-support').install();
process.on('unhandledRejection', console.log);
import {
    MatrixClient,
    SimpleFsStorageProvider,
    AutojoinRoomsMixin,
    RichReply
} from "matrix-bot-sdk";
import { LetterboxdClient } from "../lbxd/LetterboxdClient";
const config = require('config');
const path = require('path')

import * as commandHandlers from './commandHandlers';
import { sendReaction } from "./utils";

// We'll want to make sure the bot doesn't have to do an initial sync every
// time it restarts, so we need to prepare a storage provider. Here we use
// a simple JSON database.
console.log(config.storage.dirPath, config.storage.matrixPath)
const storage = new SimpleFsStorageProvider(path.join(config.storage.dirPath, config.storage.matrixPath));
const matrixClient = new MatrixClient(config.auth.homeserver, config.auth.accessToken, storage);
const lbxdClient = new LetterboxdClient();
AutojoinRoomsMixin.setupOnClient(matrixClient);

matrixClient.on("room.encrypted", (roomId, event) => {
    const replyBody = "E2E encryption not supported; see https://github.com/turt2live/matrix-bot-sdk/issues/7"; // we don't have any special styling to do.
    console.error(replyBody);
    const reply = RichReply.createFor(roomId, event, replyBody, replyBody);
    reply["msgtype"] = "m.notice";
    matrixClient.sendMessage(roomId, reply);
});

// matrixClient.on("room.event", (roomId, event) => {
//     if (!event["content"]) return;
//     console.log(roomId + ': ' + event["sender"] + " sent " + event["type"]);
//     console.log(event)
// });

matrixClient.on("room.message", handleCommand);

matrixClient.start().then(() => console.log("Client started!"));

async function filterEvents(event: any) {
    // Don't handle events that don't have contents (they were probably redacted)
    if (!event["content"]) return Promise.resolve(false);

    // Don't handle non-text events
    if (event["content"]["msgtype"] !== "m.text") Promise.resolve(false);

    // We never send `m.text` messages so this isn't required, however this is
    // how you would filter out events sent by the bot itself.
    if (event["sender"] === await matrixClient.getUserId()) return Promise.resolve(false);

    return Promise.resolve(true);
}


const regex = RegExp("(letterboxd.com/|boxd.it/)[\\w\\-\\./]+", "g");

// This is our event handler for dealing with the `!hello` command.
async function handleCommand(roomId: any, event: any) {
    if (!await filterEvents(event)) {
        return;
    }

    // format body by stripping @-tag of the bot's user
    let body: string = event["content"]["body"] ?? '';
    if (body.startsWith(`${config.auth.username}:`)) {
        body = body.substr(config.auth.username.length);
    }
    body = body.trim();
    if (body === '')  return;

    const formattedBody = body.toLowerCase() ?? '';

    if (formattedBody.startsWith('!watch')) {
       commandHandlers.watch(matrixClient, lbxdClient, body, roomId, event); 
    } else if (formattedBody.startsWith('!veto')) {
       commandHandlers.veto(matrixClient, lbxdClient, body, roomId, event); 
    } else if (formattedBody.startsWith('!help')) {
        commandHandlers.help(matrixClient, roomId, event);
    } else if (formattedBody.startsWith('!uptime')) {

    } else if (formattedBody.startsWith('!check')) {
        commandHandlers.check(matrixClient, lbxdClient, body, roomId, event);
    } else if (formattedBody.startsWith('!choose')) {
        commandHandlers.choose(matrixClient, body, roomId, event);
    } else if (
        formattedBody.startsWith('zero days') ||
        formattedBody.startsWith('0 days') ||
        formattedBody.startsWith('!osha')
    ) {
        await commandHandlers.incidentReport(matrixClient, body, roomId, event)
    } else {
        // if no command is present, grab all letterboxd/boxd.it URLs
        const urls = body.match(regex) ?? [];
        if (!urls.length) {
            return;
        }

        const watchlistId = await lbxdClient.getFilmListId(config.lbxd.lists.watchlist);
        const vetoedId = await lbxdClient.getFilmListId(config.lbxd.lists.vetoed);
        const mentionedId = await lbxdClient.getFilmListId(config.lbxd.lists.mentioned);

        const filmIdPromises = urls.map((url) => lbxdClient.getFilmId(`https://${url}`));
        const filmIds = await Promise.all(filmIdPromises);
        filmIds?.forEach(async (id) => {
            const lists = await lbxdClient.checkLists(id);
            if (lists.some((list: any) => list.id == vetoedId)) {
                sendReaction(matrixClient, roomId, event, '🙈');
                return;
            }

            const watchlistRes= await lbxdClient.addToList(watchlistId, id);
            const mentionedRes = await lbxdClient.addToList(mentionedId, id);
            if (!watchlistRes.data.result) {
                if (watchlistRes.data.errorCodes[0] === 'error.filmlist.already.contains.film') {
                    sendReaction(matrixClient, roomId, event, '👀');
                } else {
                console.error('Could not add to watchlist: ', watchlistRes.data.messages)
                const message = watchlistRes.data.messages[0];
                const reply = RichReply.createFor(roomId, event, message, message);
                reply["msgtype"] = "m.notice";
                matrixClient.sendMessage(roomId, reply);
                }
            } else {
                sendReaction(matrixClient, roomId, event, '✅');
            }
            if (!mentionedRes.data.result) {
                console.error('Could not add to mentioned list: ', mentionedRes.data.messages)
            }
        });

    }
}