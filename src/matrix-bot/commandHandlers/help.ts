import {
    MatrixClient,
    RichReply
} from "matrix-bot-sdk";

const config = require('config');
const pjson = require('../../../package.json');

function help(client: MatrixClient, roomId: string, event: any) {
    const replyBody = `(v${pjson.version}) ` +
    "All letterboxd.com or boxd.it URL are added to the " +
    `<a href="${config.lbxd.lists.watchlist}">watchlist</a> ` +
    `and <a href="${config.lbxd.lists.mentioned}">mentioned-list</a><br />` +
    "Reacts: ✅ when something's added, 👀 if already added, ⌚️ if watched, and ❌ if vetoed<br /><br />" +
    "<code>!watch url</code> Mark a movie as watched<br />" +
    "<code>!veto url [optional reason]</code> " +
        `<a href="${config.lbxd.lists.vetoed}">Vetoed</a> ` +
        "movies are prevented from being added to the watchlist<br />" +
    "<code>!check url</code> Reports which lists this movie has been added to<br />" +
    "<code>!choose [options, delimited by emoji]</code> Reacts with a random one of the emoji you sent, making a choice for you<br />" +
    "<code>!lists</code> Prints all list URLs";

    const reply = RichReply.createFor(roomId, event, replyBody, replyBody);
    reply["msgtype"] = "m.notice";

    client.sendMessage(roomId, reply);
}

export {help};
