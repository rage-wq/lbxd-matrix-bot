import { MatrixClient } from "matrix-bot-sdk";
import { LetterboxdClient } from "../../lbxd/LetterboxdClient";
import { sendReaction } from "../utils";
const config = require("config");

async function veto(client: MatrixClient, lbxdClient: LetterboxdClient, body: string, roomId: any, event: any) {
    const regex = RegExp("(letterboxd.com/|boxd.it/)[\\w\\-\\./]+", "g");
    const matches = body.match(regex);
    let replyBody;
    if (!matches || !matches[0] || matches.length !== 1) {
        replyBody = 'The !veto command requires a single letterboxd.com or boxd.it URL, plus an optional reason for veto.';
        return;
    } 

    const filmId = await lbxdClient.getFilmId(`https://${matches[0]}`);
    const vetoedId = await lbxdClient.getFilmListId(config.lbxd.lists.vetoed);
    const mentionedId = await lbxdClient.getFilmListId(config.lbxd.lists.mentioned);

    // const reason = body.substring(matches[0].length);
    // if (reason.length) {
    //   filmData.review = body.substring(matches[0].length),
    //   filmData.containsSpoilers = true;
    // }

    if (filmId) {
      await lbxdClient.addToList(vetoedId, filmId);
      await lbxdClient.addToList(mentionedId, filmId);
      await lbxdClient.removeFilmsFromList(config.lbxd.lists.watchlist, filmId);
    }

    // removeFromWatchlist()
    // markAsWatched()
    // check lists
    // if in watchlist, remove from watchlist

    sendReaction(client, roomId, event, '🙈');
  }

export {veto};
