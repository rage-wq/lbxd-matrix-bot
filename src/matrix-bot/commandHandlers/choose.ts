import { MatrixClient } from "matrix-bot-sdk";
import { RichReply } from 'matrix-bot-sdk';
//@ts-ignore
import emojiRegex from 'emoji-regex/RGI_Emoji.js';
import sendReaction from '../utils/sendReaction';
const config = require("config");

// Note: because the regular expression has the global flag set, this module
// exports a function that returns the regex rather than exporting the regular
// expression itself, to make it impossible to (accidentally) mutate the
// original regular expression.

const text = `
\u{231A}: ⌚ default emoji presentation character (Emoji_Presentation)
\u{2194}\u{FE0F}: ↔️ default text presentation character rendered as emoji
\u{1F469}: 👩 emoji modifier base (Emoji_Modifier_Base)
\u{1F469}\u{1F3FF}: 👩🏿 emoji modifier base followed by a modifier
`;


async function choose(client: MatrixClient, body: string, roomId: any, event: any) {
    const regex = emojiRegex();
    const matches = body.match(regex);
  
    if (!matches || matches.length === 0) {
        const replyBody = "To use !choose, send multiple options separated by emoji.\n" +
        "I'll react with a choice and then it's nobody's fault we watched Monkeybone.";
        const reply = RichReply.createFor(roomId, event, replyBody, replyBody);
        reply["msgtype"] = "m.notice";

        client.sendMessage(roomId, reply);
        return;
    } 

    const choiceIndex = Math.floor(Math.random() * matches.length)
    const choice = matches[choiceIndex];
    sendReaction(client, roomId, event, choice);
}

export {choose};
