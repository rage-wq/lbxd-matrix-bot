import { MatrixClient } from "matrix-bot-sdk";
import { LetterboxdClient } from "../../lbxd/LetterboxdClient";
import { sendReaction } from "../utils";
const config = require("config");


async function watch(client: MatrixClient, lbxdClient: LetterboxdClient, body: string, roomId: any, event: any) {
    const regex = RegExp("(letterboxd.com/|boxd.it/)[\\w\\-\\./]+", "g");
    const matches = body.match(regex);
    let replyBody;
    if (!matches || matches.length === 0) {
        replyBody = 'The !watch command requires a single letterboxd.com or boxd.it URL';
        return;
    } 

    let filmIdPromises = matches.map((url) => lbxdClient.getFilmId(`https://${url}`));

    console.log(filmIdPromises);
    const filmIds = await Promise.all(filmIdPromises);

    await lbxdClient.addFilmsToList(config.lbxd.lists.mentioned, filmIds);
    await lbxdClient.removeFilmsFromList(config.lbxd.lists.watchlist, filmIds);

    sendReaction(client, roomId, event, '⌚️');
}

export {watch};