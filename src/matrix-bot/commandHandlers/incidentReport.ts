import {RichReply} from 'matrix-bot-sdk';
const config = require('config');

// @ts-ignore
import RelativeTimeFormat from "relative-time-format"
const en = require("relative-time-format/locale/en.json");
RelativeTimeFormat.addLocale(en);

import sendReaction from '../utils/sendReaction';

const fs = require('fs');
const path = require ( 'path' );
const reportFile = path.join(config.storage.dirPath, config.storage.incidentPath);

const MSEC_PER_MIN = 1000*60;
const MSEC_PER_HR = MSEC_PER_MIN * 60;
const MSEC_PER_DAY = MSEC_PER_HR*24;

async function incidentReport(client: any, body: string, roomId: any, event: any) {
    const rtf = new RelativeTimeFormat('en', { style: 'long' });

    sendReaction(client, roomId, event, "👀");

    let now = Date.now();

    let prettyPrevIncident = 'No previous incident';

    fs.readFile(reportFile, 'utf-8', (err: any, data: any) => {
        if (err) {
            console.log(`${reportFile} does not exist`);
        }

        if (data && data !== '') {
            const prevIncident = new Date(parseInt(data, 10));
            const timeElapsed = (now - prevIncident.getTime());
            if (timeElapsed < MSEC_PER_MIN) {
                prettyPrevIncident = rtf.format(-1*Math.floor(timeElapsed/1000), 'seconds')
            } else if (timeElapsed < MSEC_PER_HR) {
                prettyPrevIncident = rtf.format(-1*Math.floor(timeElapsed/MSEC_PER_MIN), 'minutes')
            } else if (timeElapsed < MSEC_PER_DAY) {
                prettyPrevIncident = rtf.format(-1*Math.floor(timeElapsed/MSEC_PER_HR), 'hours')
            } else {
                prettyPrevIncident = rtf.format(-1*Math.floor(timeElapsed/MSEC_PER_DAY), 'days')
            }
        }

        let replyBody = ''
        if (!body.startsWith('!osha')) {
            replyBody = '🚨 Thank you for reporting this incident. ';
            fs.writeFile(
                reportFile, now.toString(), { flag: 'w' },
                (err: any) => {if (err) console.log(err);}
            );
        }
        replyBody += `Our previous incident was ${prettyPrevIncident}. `

        const reply = RichReply.createFor(roomId, event, replyBody, replyBody);
        reply["msgtype"] = "m.notice";

        client.sendMessage(roomId, reply);
    });

}

export {incidentReport};