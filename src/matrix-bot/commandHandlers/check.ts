import { MatrixClient, RichReply } from "matrix-bot-sdk";
import { LetterboxdClient } from "../../lbxd/LetterboxdClient";

async function check(client: MatrixClient, lbxdClient: LetterboxdClient, body: string, roomId: any, event: any) {
    const regex = RegExp("(letterboxd.com/|boxd.it/)[\\w\\-\\./]+", "g");
    const matches = body.match(regex);
  
    if (!matches || matches.length === 0) {
        const replyBody = "To use !check, send a movie URL after the command.\n" +
        "I'll reply with all lists the bot account maintains that include the movie.";
        const reply = RichReply.createFor(roomId, event, replyBody, replyBody);
        reply["msgtype"] = "m.notice";

        client.sendMessage(roomId, reply);
        return;
    } 

    const filmId = await lbxdClient.getFilmId(`https://${matches[0]}`);
    const lists = await lbxdClient.checkLists(filmId);
    let replyBody = '';
    lists.forEach(list => {
      replyBody += `#${list.position+1} of ${list.numEntries} in <a href="https://boxd.it/${list.boxdItCode}">${list.name}</a>`
      if (list.private) {
        replyBody += ' (private list)';
      }
      replyBody += '<br />';
    })

    const reply = RichReply.createFor(roomId, event, replyBody, replyBody);
    reply["msgtype"] = "m.notice";

    client.sendMessage(roomId, reply);
}

export {check};