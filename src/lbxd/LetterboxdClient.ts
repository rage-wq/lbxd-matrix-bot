import axios from "axios";
import { listenerCount } from "stream";
import { CookieJar } from "tough-cookie";
const config = require("config");
const axiosCookieJarSupport = require("axios-cookiejar-support").default;

import * as actions from './actions';

class LetterboxdClient {
  cookieJar: CookieJar;
  csrf: string;

  constructor() {
    axiosCookieJarSupport(axios);
    this.cookieJar = new CookieJar();
    this.csrf = 'Not supplied';

    actions.getCsrf(this.cookieJar)
      .then((csrf) => {
        console.log('getCsrf', csrf);
        this.csrf = csrf;
      })
      .catch(() => {
        throw 'Unable to obtain CSRF';
      })
      .then(() => actions.login(config.lbxd.username, config.lbxd.password, this.csrf, this.cookieJar))
      .then((res) => {
        if (res.data.result === "error") {
          console.error(`Letterboxd login failed: "${res.data.messages}"`);
          console.error(
            `It's likely this is due to bad CSRF. We sent ${this.csrf}, they wanted ${res.data.csrf}`
          );
        } else {
          console.log(`Logged in as ${config.lbxd.username}`);
        }
      })
      .catch((err) => {
        console.error(`Letterboxd login failed: generic failure ${err}`);
        return 1;
      });
    
    
  }

  async getFilmId(url: string) {
    return await actions.getFilmId(url, this.cookieJar);
  }

  async getLastListUpdate(listUrl: string) {
    return await actions.getLastListUpdate(listUrl, this.cookieJar);
  }

  async getListContents(listUrl: string) {
    return await actions.getListContents(listUrl, this.cookieJar);
  }

  async getFilmListId(listUrl: string) {
    return await actions.getFilmListId(listUrl, this.cookieJar);
  }

  async checkLists(filmId: string) {
    return await actions.checkLists(filmId, this.csrf, this.cookieJar);
  }

  async addToList(listId: string, filmId: string) {
    return await actions.addToList(listId, filmId, this.csrf, this.cookieJar);
  }


  async addFilmsToList(listUrl: string, idsToAdd: Array<string> | string) {
    return await actions.removeFilmsFromList(listUrl, idsToAdd, this.csrf, this.cookieJar);
  }

  async removeFilmsFromList(listUrl: string, idsToRemove: Array<string> | string) {
    return await actions.removeFilmsFromList(listUrl, idsToRemove, this.csrf, this.cookieJar);
  }
}

export {LetterboxdClient};