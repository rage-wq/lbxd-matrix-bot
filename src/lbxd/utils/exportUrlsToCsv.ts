
const createCsvWriter = require('csv-writer').createObjectCsvWriter;

function exportUrlsToCsv(urls: Array<string>) {
  const csvWriter = createCsvWriter({
    path: 'lbxd-import.csv',
    header: [
      {id: 'url', title: 'LetterboxdURI'},
    ]
  });

  const data = urls.map(url => ({url}));

  csvWriter
    .writeRecords(data)
    .then(()=> console.log('The CSV file was written successfully'));
}

export {exportUrlsToCsv};