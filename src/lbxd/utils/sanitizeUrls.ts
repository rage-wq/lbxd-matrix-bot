
function sanitizeUrls(unsanitizedUrls: Array<string>) {
  return Array.from(
    new Set(unsanitizedUrls
      .filter((url: string) =>
        !url.includes('/list/') &&
        !url.includes('/api-coming-soon')
      )
      .map((url: string) =>
        url
        .replace('/trailer', '/')
        .replace('/.*/film/', '/film/')
      )
    )
  );
}

export {sanitizeUrls};