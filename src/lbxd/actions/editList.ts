import { stringify } from "querystring";
import {CookieJar} from "tough-cookie";
const querystring = require("querystring");
const axios = require("axios");

type ListEntry = {
    filmId: string;
    review?: string; // note on the list
    containsSpoilers?: boolean; // note on the list
}

type ListData = {
    entries: Array<ListEntry>
    filmListId: string;
    name: string,
    tags: Array<string>,
    publicList: boolean,
    notes: string,
}

// stolen from https://stackoverflow.com/questions/26311947/javascript-equivalent-to-urlencoder-encodestring-utf-8-of-java
function javaURLEncode(str: string) {
  return encodeURI(str)
    .replace(/%20/g, "+")
    .replace(/!/g, "%21")
    .replace(/'/g, "%27")
    .replace(/\(/g, "%28")
    .replace(/\)/g, "%29")
    .replace(/~/g, "%7E")
    .replace(/\$/g, "%24");
}

function editList(
    {
        entries,
        filmListId,
        name,
        tags,
        publicList,
        notes,
    }: ListData,
    csrf: string,
    cookieJar: CookieJar
) {
    const data = {
        filmListId,
        entries: JSON.stringify(entries),
        __csrf: csrf,
    }

    if (!publicList) {
      //@ts-ignore
      data.publicList = 'false';
    }

    let stringifiedData = querystring.stringify(data);

    stringifiedData += `&name=${javaURLEncode(name)}` +
      `&notes=${javaURLEncode(notes)}` +
      `&tags=${'&tag=' + tags.join('&tag=')}`
    console.log(data);

    return axios.post("https://letterboxd.com/s/save-list", stringifiedData, {
        headers: {
            post: {
                Host: "letterboxd.com",
                Origin: "https://letterboxd.com",
                Referer: "https://letterboxd.com",
                "User-Agent":
                  "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0",
                "Sec-GPC": "1",
                DNT: "1",
                TE: "Trailers",
                "X-Requested-With": "XMLHttpRequest",
            },
        },
        withCredentials: true,
        jar: cookieJar,
    });
}

export {editList};
