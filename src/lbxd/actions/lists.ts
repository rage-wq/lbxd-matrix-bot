import { CookieJar } from 'tough-cookie';
import {editList} from './editList';
import {getListContents} from './getListContents';

async function addFilmsToList(listUrl: string, idsToAdd: Array<string> | string, csrf: string, cookieJar: CookieJar) {
  const {filmIds, newCsrf, ...listData} = await getListContents(listUrl, cookieJar);
  let updatedFilms = filmIds;
  if (typeof idsToAdd === typeof 'string') {
    updatedFilms.push(idsToAdd as string);
  } else {
    updatedFilms.push(...idsToAdd as string[]);
  }

  const entries = updatedFilms.map((id) => ({filmId: id}));
  return await editList({entries, ...listData}, newCsrf ?? csrf, cookieJar);
}

async function removeFilmsFromList(listUrl: string, idsToRemove: Array<string> | string, csrf: string, cookieJar: CookieJar) {
  const {filmIds, newCsrf, ...listData} = await getListContents(listUrl, cookieJar);
  const updatedFilms = filmIds.filter(film => !idsToRemove.includes(film))
  const entries = updatedFilms.map((id) => ({filmId: id}));
  return await editList({entries, ...listData}, newCsrf ?? csrf, cookieJar);
}

export { addFilmsToList, removeFilmsFromList };
