import {CookieJar} from "tough-cookie";
const querystring = require("querystring");
const axios = require("axios");

const ADD = true;
const REMOVE = false;

function watchlistAction(
  add: boolean = true,
  filmTitle: string,
  csrf: string,
  cookieJar: CookieJar
) {
  const data = querystring.stringify({
    __csrf: csrf,
  });

  return axios.post(`https://letterboxd.com/film/${filmTitle}/${add ? 'add-to-' : 'remove-from-'}watchlist/`, data, {
    headers: {
      post: {
        Host: "letterboxd.com",
        Origin: "https://letterboxd.com",
        Referer: "https://letterboxd.com",
        "User-Agent":
          "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0",
        "Sec-GPC": "1",
        DNT: "1",
        TE: "Trailers",
        "X-Requested-With": "XMLHttpRequest",
      },
    },
    withCredentials: true,
    jar: cookieJar,
  });
}


function addToWatchlist(
  filmTitle: string,
  csrf: string,
  cookieJar: CookieJar
) {
  return watchlistAction(ADD, filmTitle, csrf, cookieJar);
}

function removeFromWatchlist(
  filmTitle: string,
  csrf: string,
  cookieJar: CookieJar
) {
  return watchlistAction(REMOVE, filmTitle, csrf, cookieJar);
}

export {addToWatchlist, removeFromWatchlist};