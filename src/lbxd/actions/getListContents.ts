import { CookieJar } from "tough-cookie";
import * as cheerio from "cheerio";
const axios = require("axios");

const regex = RegExp("csrf=([a-z0-9]+)", "i");

async function getListContents(listUrl: string, cookieJar: CookieJar) {
  // const lastUpdateTime = await getLastListUpdate(listUrl, cookieJar);
  const res = await axios.get(`${listUrl}/edit`, {jar: cookieJar, withCredentials: true});
  const $ = cheerio.load(res.data);

  // nb: $('li[class=film-list-entry]) doesn't work here for some reason.
  const filmIds = $('li.film-list-entry')
    .map((_, el) => $(el).attr('data-film-id')).toArray();
  const title = $('meta[property=og:title]')?.attr('content')?? '';
  const startOfName = title.indexOf("‘")+ 1;
  const endOfName = title.lastIndexOf("’");
  const name = title.substring(startOfName, endOfName);
  const notes = $('textarea[name=notes]').text();
  const publicList = ($('input[name=publicList]').attr('value') ?? 'true') === 'true';
  const tags = $('ul.tags li.tag input')
    .map((_, el) => $(el).attr('value')).toArray();
	const filmListId = $('input[name=filmListId]').attr('value') ?? '';

  // @ts-ignore
	const newCsrf = $('form#list-form input[name=__csrf]').attr('value');

  return {
    filmListId,
    name,
    tags,
    publicList,
    notes,
    filmIds,
    newCsrf,
  };
};

export {getListContents};