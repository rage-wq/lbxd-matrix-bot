import {CookieJar} from "tough-cookie";
const axios = require("axios");

async function getFilmListId(listUrl: string, cookieJar: CookieJar) {
  const regex = RegExp("filmlist:([0-9]+)");
  const res = await axios.get(listUrl, {jar: cookieJar, withCredentials: true});

  // @ts-ignore
  return regex.exec(res.headers["xkey"])[1];
}

export {getFilmListId};