import {CookieJar} from "tough-cookie";
const axios = require("axios");

async function getCsrf(cookieJar: CookieJar) {
  const get = await axios.get("https://letterboxd.com/", {
    jar: cookieJar,
    withCredentials: true,
  });
  const regex = RegExp("csrf=([a-z0-9]+)", "i");
  // @ts-ignore
  return regex.exec(get.headers["set-cookie"])[1];
}

export {getCsrf};