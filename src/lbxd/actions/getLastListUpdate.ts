import {CookieJar} from "tough-cookie";
import * as cheerio from 'cheerio';
const axios = require("axios");

async function getLastListUpdate(listUrl: string, cookieJar: CookieJar) {
  const res = await axios.get(listUrl, {jar: cookieJar, withCredentials: true});
  const $ = cheerio.load(res.data);

  // nb: $('.published) doesn't work here for some reason.
  const published = $('span[class=published] time').attr('datetime')
  const updated = $('span[class=updated] time').attr('datetime')

  return updated ?? published;
}

export {getLastListUpdate};