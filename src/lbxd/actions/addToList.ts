import {CookieJar} from "tough-cookie";
const querystring = require("querystring");
const axios = require("axios");

function addToList(
  filmListId: string,
  filmId: string,
  csrf: string,
  cookieJar: CookieJar
) {
  const data = querystring.stringify({
    filmId,
    filmListId,
    __csrf: csrf,
  });

  return axios.post("https://letterboxd.com/s/add-film-to-list", data, {
    headers: {
      post: {
        Host: "letterboxd.com",
        Origin: "https://letterboxd.com",
        Referer: "https://letterboxd.com",
        "User-Agent":
          "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0",
        "Sec-GPC": "1",
        DNT: "1",
        TE: "Trailers",
        "X-Requested-With": "XMLHttpRequest",
      },
    },
    withCredentials: true,
    jar: cookieJar,
  });
}
export {addToList};