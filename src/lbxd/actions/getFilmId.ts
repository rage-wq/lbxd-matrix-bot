import {CookieJar} from "tough-cookie";
const axios = require("axios");

async function getFilmId(filmUrl: string, cookieJar: CookieJar) {
  const regex = RegExp("film:([0-9]+)");
  const res = await axios.get(filmUrl, {jar: cookieJar, withCredentials: true});

  // @ts-ignore
  return regex.exec(res?.headers["xkey"])[1];
}

export {getFilmId};