import {CookieJar} from "tough-cookie";

const prompts = require("prompts");
const config = require("config");

import {getFilmListId} from '../actions';

const listQuestion = [
  {
    type: config?.lbxd?.list ? null : "text",
    name: "listUrl",
    message: "Enter list URL",
  },
];

async function promptForList(cookieJar: CookieJar) {
  if (!config?.lbxd?.lists.mentioned) {
    console.info(
      "You can avoid entering a list each time by setting `lbxd.lists.mentioned`"
    );
  }
  const response = (await prompts(listQuestion));
  const listUrl = config?.lbxd?.lists?.mentioned || response.listUrl
  const filmListId = await getFilmListId(listUrl, cookieJar);
  return {listUrl, filmListId};
}

export {promptForList};