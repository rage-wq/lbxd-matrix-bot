import {CookieJar} from "tough-cookie";

const prompts = require("prompts");
const config = require("config");

import {login} from '../actions';

const loginQuestions = [
  {
    type: config?.lbxd?.password ? null : "text",
    name: "username",
    message: "Enter Letterboxd username",
  },
  {
    type: config?.lbxd?.password ? null : "invisible",
    name: "password",
    message: "Password:",
  },
];

async function promptForLogin(csrf: string, cookieJar: CookieJar) {
  if (!config?.lbxd?.username || !config?.lbxd?.password) {
    console.info(
      "You can avoid logging in each time by setting `lbxd.username` and `lbxd.password` in config"
    );
  }

  const response = await prompts(loginQuestions);
  const username = config?.lbxd?.username || response.username;
  const password = config?.lbxd?.password || response.password;

  try {
    const res = await login(username, password, csrf, cookieJar);
    if (res.data.result === "error") {
      console.error(`Letterboxd login failed: "${res.data.messages}"`);
      console.error(
        `It's likely this is due to bad CSRF. We sent ${csrf}, they wanted ${res.data.csrf}`
      );
      return 1;
    }
  } catch (err) {
    console.error(`Letterboxd login failed: generic failure ${err}`);
    return 1;
  }
  console.log(`Logged in as ${username}`);
}

export {promptForLogin};
