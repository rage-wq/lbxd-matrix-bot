const prompts = require("prompts");

async function promptForCsvMode(numUrls: number) {
  const {exportCsv} = await prompts({
    type: 'confirm',
    name: 'exportCsv',
    message: `Scanned ${numUrls} links. 'Yes' to export an importable CSV, 'No' to make ${numUrls} individual add requests.`,
    initial: true
  });

  return exportCsv;
}

export {promptForCsvMode};
