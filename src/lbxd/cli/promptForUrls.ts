const prompts = require("prompts");
const clipboardy = require("clipboardy");
import { readFileSync } from "fs";

async function promptForUrls() {
  const {method} = (await prompts({
    type: 'select',
    name: 'method',
    message: 'Choose how to supply URLs',
    choices: [
      { title: 'Copy from clipboard', description: 'Copies as soon as this is selected', value: 'clip' },
      { title: 'Read from file', description: 'UTF-8 text, any filetype', value: 'file' },
      { title: 'Enter as text', value: 'text'},
    ],
    initial: 1
  }));

  let urls;
  switch(method) {
    case 'clip':
      // grab URLs from clipboard-- more methods coming later
      console.info("Reading movies from your clipboard...");
      urls = clipboardy.readSync();
      break;
    case 'file':
      const { path } = await prompts({
        type: 'text',
        name: 'path',
        message: 'Enter path to file containing URLs',
      });
      urls = readFileSync(path, 'utf8');
      break;
    case 'text':
    default:
      urls= await prompts({
        type: 'text',
        name: 'urls',
        message: 'Enter letterboxd.com or boxd.it URLs, separated by spaces',
      }).urls ?? '';
      break;
  }

  const regex = RegExp("(letterboxd.com/|boxd.it/)[\\w\\-\\./]+", "g");
  return urls.match(regex);
}

export {promptForUrls};