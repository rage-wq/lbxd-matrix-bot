const axios = require("axios");
const axiosCookieJarSupport = require("axios-cookiejar-support").default;
const tough = require("tough-cookie");

import * as actions from './actions';
import * as cli from './cli';
import * as utils from './utils';

// Uncomment to get details on all axios ops, can be placed right before debug
/*
  axios.interceptors.request.use(function (config) {
    // Do something before request is sent
    console.log(config);
    return config;
  }, function (error) {
    // Do something with request error
    return Promise.reject(error);
  });
*/

async function main() {
  // LBXD uses CSRF cookies and session cookies for auth
  axiosCookieJarSupport(axios);
  const cookieJar = new tough.CookieJar();
  const csrf = await actions.getCsrf(cookieJar);

  await cli.promptForLogin(csrf, cookieJar);
  const {filmListId, listUrl} = await cli.promptForList(cookieJar);
  const unsanitizedUrls = await cli.promptForUrls();

  if (!unsanitizedUrls) {
    console.error('No URLs found.');
    return 1;
  }
  const urls = utils.sanitizeUrls(unsanitizedUrls); 
  if (!urls || !urls.length) {
    console.error('No URLs found after sanitizing.');
    return 1;
  }

  // CSV export preferred; it's easy to hit their rate limit
  // in the future, we'll use their list-edit mode rather than indiv. add to avoid this
  const exportCsv = await cli.promptForCsvMode(urls.length);

  if (exportCsv) {
    utils.exportUrlsToCsv(urls);
  } else {
    urls.forEach(async (url) => {
      try {
        const filmId = await actions.getFilmId(`https://${url}`, cookieJar);
        const res = await actions.addToList(filmListId, filmId, csrf, cookieJar);
        if (res.status === 200 && res.data.result === false) {
          console.error(`Error: Letterboxd refused to add ${url}`);
        }
        console.log(`Added ${url} to ${listUrl}`);
      } catch (err) {
        console.error(`Error adding ${url}: ${err}`);
      }
    });
  }
}

main();
